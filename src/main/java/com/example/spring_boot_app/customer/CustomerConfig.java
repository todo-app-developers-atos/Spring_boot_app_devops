package com.example.spring_boot_app.customer;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CustomerConfig {

    private final CustomerRepository repo ;

    public CustomerConfig(CustomerRepository repo) {
        this.repo = repo;
    }

    @Bean
    CommandLineRunner init(){
        return args -> {
            repo.save(new Customer(null,"chebihi","f.chebihi@gmail.com"));
            repo.save(new Customer(null,"chebihi2","f.chebihi2@gmail.com"));
        };
    }
}
