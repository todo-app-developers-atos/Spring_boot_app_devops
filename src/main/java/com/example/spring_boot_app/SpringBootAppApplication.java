package com.example.spring_boot_app;

import com.example.spring_boot_app.customer.Customer;
import com.example.spring_boot_app.customer.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringBootAppApplication {


    public static void main(String[] args) {
        SpringApplication.run(SpringBootAppApplication.class, args);
    }



}
